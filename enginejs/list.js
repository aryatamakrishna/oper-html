var lists = [
  {
    "name": "Homepage",
    "path": "index.html"
  },
  {
    "name": "Login",
    "path": "index.html?view=login"
  },
  {
    "name": "Player Detail",
    "path": "index.html?view=player-detail"
  },
  {
    "name": "search",
    "path": "index.html?view=search"
  },
  {
    "name": "Create Team",
    "path": "index.html?view=create-team"
  },
  {
    "name": "Create Challange Match",
    "path": "index.html?view=create-match-challange"
  },
  {
    "name": "Create Fun Match",
    "path": "index.html?view=create-match-fun"
  },
  {
    "name": "Arena Search",
    "path": "index.html?view=arena-search"
  },
  {
    "name": "Arena Detail",
    "path": "index.html?view=arena-detail"
  },
  {
    "name": "Klasemen",
    "path": "index.html?view=klasemen"
  },
  {
    "name": "My Match",
    "path": "index.html?view=mymatch"
  },
  {
    "name": "Team Detail",
    "path": "index-login.html?view=team-detail"
  },
  {
    "name": "Team Gallery",
    "path": "index.html?view=team-gallery"
  },
  {
    "name": "Single Gallery",
    "path": "index.html?view=single-team-gallery"
  },
  {
    "name": "Setting",
    "path": "index.html?view=setting"
  },
  {
    "name": "Notifications",
    "path": "index-login.html?view=notification"
  },
  {
    "name": "Match Detail Challange",
    "path": "index.html?view=match-detail"
  },
  {
    "name": "Match Detail Fun",
    "path": "index.html?view=match-detail-fun"
  },
  {
    "name": "Match Detail Queue",
    "path": "index.html?view=match-detail-queue"
  },
  {
    "name": "Email - Signup",
    "path": "email-signup.html"
  },
  {
    "name": "Email - Invite Play",
    "path": "email-invite-play.html"
  },
  {
    "name": "Email - Join Team",
    "path": "email-join-team.html"
  },
  {
    "name": "Email - Forgot Password",
    "path": "email-forgotpassword.html"
  }
];
