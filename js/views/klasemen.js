// Klasemen Table Woman
$(".js-ktw").DataTable({
	dom: 'frtp',
});

// Klasemen Table Man
$(".js-ktm").DataTable({
	dom: 'frtp',
});

$(".klasemen-wrap .tabs-wrap a").on("click", function(e){
	e.preventDefault();
	var href = $(this).attr("href");

	$(".klasemen-wrap .tabs-wrap a").removeClass("active");
	$(this).addClass("active");

	$(".klasemen-gender").addClass("hideit");
	$(href).removeClass("hideit");
});