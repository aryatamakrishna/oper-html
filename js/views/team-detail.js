functions.push(function(){
	// Begin - for popup photo
	var winWidth = $(window).width();
	var winHeight = $(window).height();
	var winHeight = winHeight - 200;

	$("#modal-gallerySlider .img-wrap img").css("height", winHeight);
	$("#modal-gallerySlider .comment-wrap").css("height", winHeight);

	$("#allPhotos .teamGallery-item").on("click", function(e){
		e.preventDefault();
		var index = $(this).index();
		var path = $(this).attr("data-img");

		$("#modal-gallerySlider").addClass("active");
		$(".innerGallery-slide").slick({
			dots: false,
		    infinite: false,
		    autoplay: false,
		    fade:true,
			prevArrow: '<div class="arrow-right" style="display:block;"><i class="lnr lnr-chevron-right"></i></div>',
			nextArrow: '<div class="arrow-left" style="display:block;"><i class="lnr lnr-chevron-left"></i></div>'
		});
		$(".innerGallery-slide").slick("slickGoTo", parseInt(index));
		
	});

	$("#modal-gallerySlider .background-wrap").on("click", function(e){
		e.preventDefault();

		$(".innerGallery-slide").slick("destroy");
		$("#modal-gallerySlider").removeClass("active");
	});

	// End - for popup photo
	
	// Begin - Mobile Site
	if( winWidth < 992){
		$(".mobile-tabs-content li a").on("click", function(){
			var target = $(this).attr("target");

			$(".mobile-tabs-content li a").removeClass("active");
			$(this).addClass("active");

			$(".mobile-tabs-trigger").addClass("mbl-hideit");
			$(target).removeClass("mbl-hideit");
		});
	}
	// End - Mobile Site

	// Begin - etc
	$(".teamGallery-menu ul li a").on("click", function(e){
		e.preventDefault();
		var href = $(this).attr("href");

		$(".teamGallery-menu ul li a").removeClass("active");
		$(this).addClass("active");

		$(".teamGallery-list").addClass("hideit");
		$(href).removeClass("hideit");
	});


	$(".add-item").dropzone({ url: "/file/post" });
	// End - etc

});