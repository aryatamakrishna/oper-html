// alert('hello');
functions.push(function(){
	$(".klasemen-wrap .tabs-wrap a").on("click", function(e){
		e.preventDefault();
		var href = $(this).attr("href");

		$(".klasemen-wrap .tabs-wrap a").removeClass("active");
		$(this).addClass("active");

		$(".klasemen-gender").addClass("hideit");
		$(href).removeClass("hideit");
	});

	// ----> Slider Hero Image
	$(".hero-wrap").slick({
	    dots: false,
	    infinite: true,
	    customPaging: function(slider, i) {
	      return '<div class="hero-paging"></div>';
	    },
		focusOnSelect:false,
		prevArrow: '<div class="arrow-left"><i class="lnr lnr-chevron-left"></i></div>',
		nextArrow: '<div class="arrow-right"><i class="lnr lnr-chevron-right"></i></div>',
	});
});
