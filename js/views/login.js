functions.push(function(){
	// Dropzone JS
	$("#dz-profpic").dropzone({
		url:"/profpic-upload", 
		maxFilesize:10,
		maxFiles:1,
		init: function(){
			this.on("queuecomplete", function(file, res){
				// $("body").css("overflow", "hidden");
				$(".dropzone").hide();
				$(".cropper-area").removeClass("hideit");
			});
		}
	});



	// Cropper JS
	$(".cropper-nav a").on("click", function(e){
		e.preventDefault();
	});
	$("#imgCrop").cropper({
		aspectRatio: 1 / 1,
		autoCropArea: 1
		// built: function(){
		// 	$("#cropZoomP").cropper("zoom", 0.1);
		// 	$("#cropZoomM").cropper("zoom", -0.1);
			
		// }
	});
	$("#cropZoomP").on("click", function(){
		$("#imgCrop").cropper("zoom", 0.1);
	});
	$("#cropZoomM").on("click", function(){
		$("#imgCrop").cropper("zoom", -0.1);
	});
	$("#cropLeft").on("click", function(){
		$("#imgCrop").cropper("move", -10, 0);
	});
	$("#cropRight").on("click", function(){
		$("#imgCrop").cropper("move", 10, 0);
	});
	$("#cropUp").on("click", function(){
		$("#imgCrop").cropper("move", 0, -10);
	});
	$("#cropDown").on("click", function(){
		$("#imgCrop").cropper("move", 0, 10);
	});
	
	$(".sign-wrap .tabs-wrap a").on("click", function(e){
		e.preventDefault();
		var href = $(this).attr("href");

		$(".sign-wrap .tabs-wrap a").removeClass("active");
		$(this).addClass("active");

		$(".form-wrap").addClass("hideit");
		$(href).removeClass("hideit");
	});

	$("#forgotButton").on("click", function(){
		$(this).parents(".form-wrap").addClass("hideit");

		$("#forgotpass").removeClass("hideit");
	});

	$(".image-browse input").on("change", function(){
		var file = $(this).files[0].name;
		console.log(file);
		$(this).parents(".image-browse").find("p").text(file);
	});

	// Datepicker
	 $(".birth-picker").datepicker({
	    format: 'dd/mm/yyyy',
	    changeMonth:true,
	    changeYear:true,
	    yearRange: '-100:+0'
	 }); 
});