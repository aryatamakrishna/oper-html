functions.push(function(){
	// Dropzone JS
	$("#dz-profpic").dropzone({
		url:"/profpic-upload", 
		maxFilesize:10,
		maxFiles:1,
	    uploadMultiple: false,
		init: function(){
			this.on("queuecomplete", function(file, res){
				// $("body").css("overflow", "hidden");	
				// console.log(file);
			});
			this.on("complete", function(file){
				// console.log(file);
				setTimeout(function(){
					var target = $("#dz-profpic .dz-image").find("img").attr("src");
					$(".dropzone").hide();
					$(".cropper-area").removeClass("hideit");
					$("#imgCrop").attr("src", target);
					$("#imgCrop").cropper({
						aspectRatio: 1 / 1,
						autoCropArea: 1
					});
				}, 100);
			});
			this.on("sending", function(file, xhr, formData){
				// console.log(file);
				// console.log(xhr);
				// console.log(formData);
			});
		}
	});



	// Cropper JS
	$(".cropper-nav a").on("click", function(e){
		e.preventDefault();
	});
	// $("#imgCrop").cropper({
	// 	aspectRatio: 1 / 1,
	// 	autoCropArea: 1
	// });
	$("#cropZoomP").on("click", function(){
		$("#imgCrop").cropper("zoom", 0.1);
	});
	$("#cropZoomM").on("click", function(){
		$("#imgCrop").cropper("zoom", -0.1);
	});
	$("#cropLeft").on("click", function(){
		$("#imgCrop").cropper("move", -10, 0);
	});
	$("#cropRight").on("click", function(){
		$("#imgCrop").cropper("move", 10, 0);
	});
	$("#cropUp").on("click", function(){
		$("#imgCrop").cropper("move", 0, -10);
	});
	$("#cropDown").on("click", function(){
		$("#imgCrop").cropper("move", 0, 10);
	});
});