functions.push(function(){
// This is for Range Slide 
// Begin
	$(".form-rangeInput").jRange({
		from: 1000,
		to: 1000000,
		// scale: [1,25,50,75,100],
		isRange : true,
		format:"Rp. %s"
	});
	$('.form-rangeInput').jRange('setValue', '200000,800000');

// End

	$(".arena-banner").slick({
	    dots: false,
	    infinite: true,
	    customPaging: function(slider, i) {
	      return '<div class="hero-paging"></div>';
	    },
		focusOnSelect:false,
		prevArrow: '<div class="arrow-left"><i class="lnr lnr-chevron-left"></i></div>',
		nextArrow: '<div class="arrow-right"><i class="lnr lnr-chevron-right"></i></div>',
	});
});