functions.push(function(){
	// Dropzone JS
	$("#dz-profpic").dropzone({
		url:"/profpic-upload", 
		maxFilesize:10,
		maxFiles:1,
		init: function(){
			this.on("queuecomplete", function(file, res){
				// $("body").css("overflow", "hidden");
				$(".dropzone").hide();
				$(".cropper-area").removeClass("hideit");
			});
		}
	});



	// Cropper JS
	$(".cropper-nav a").on("click", function(e){
		e.preventDefault();
	});
	$("#imgCrop").cropper({
		aspectRatio: 1 / 1,
		autoCropArea: 1
		// built: function(){
		// 	$("#cropZoomP").cropper("zoom", 0.1);
		// 	$("#cropZoomM").cropper("zoom", -0.1);
			
		// }
	});
	$("#cropZoomP").on("click", function(){
		$("#imgCrop").cropper("zoom", 0.1);
	});
	$("#cropZoomM").on("click", function(){
		$("#imgCrop").cropper("zoom", -0.1);
	});
	$("#cropLeft").on("click", function(){
		$("#imgCrop").cropper("move", -10, 0);
	});
	$("#cropRight").on("click", function(){
		$("#imgCrop").cropper("move", 10, 0);
	});
	$("#cropUp").on("click", function(){
		$("#imgCrop").cropper("move", 0, -10);
	});
	$("#cropDown").on("click", function(){
		$("#imgCrop").cropper("move", 0, 10);
	});
	
	// This is Choosing player for add and delete player in user team 
	// Begin
	$(".add-player").on("click", function(e){
		e.preventDefault();

		var name = $(this).parents(".player-wrap").attr("data-name");
		var playerCard = $(this).parents(".player-wrap").html();
		var playerInput = $(".create-team").find("input[name=playerInput\\[\\]]");

		$(this).parents(".player-wrap").remove();
		$(".result-player").append("<div class='player-wrap' data-name='"+name+"'>"+playerCard+"</div>");
		playerInput.val(playerInput.val() + ","+name);

		$(".remove-player").on("click", function(e){
			e.preventDefault();

			var input = $(".create-team #inputPlayer").val();
			input = input.split(",");
			var playerDelete = $(this).parents(".player-wrap").attr("data-name");

			input = jQuery.grep(input, function(value) {
			  return value != playerDelete;
			});

			$(".create-team #inputPlayer").val(input);

			$(this).parents(".player-wrap").remove();
			// input = $.grep(input, function(value) {
			//   return value != playerDelete;
			// });
			// alert("This is "+input);
		});
	});
});