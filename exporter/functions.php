<?php
function xcopy($source, $dest, $permissions = 0755, $config = array())
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source) ) {
      $file = substr($source, strrpos($source, '/')+1);
      if(in_array($file, $config))
        return;
      else
        return copy($source, $dest);
    }

    // Make destination directory

    $pos_1 = strrpos($source, '/')+1;
    $file1 = substr($source, strrpos($source, '/')+1);
    $file2 = substr($source, 0, $pos_1-1);
    $file2 = substr($source, strrpos($source, '/', (strrpos($source, '/')-1)-strlen($source))+1);
    if(in_array($file1, $config) || in_array($file2, $config))
      return;

    if (!is_dir($dest)) {
        mkdir($dest, $permissions, true);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        xcopy("$source/$entry", "$dest/$entry", $permissions,$config);
    }

    // Clean up
    $dir->close();
    return true;
}

function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    // $files = glob($dirPath . '*', GLOB_MARK);
    $files = array_diff(scandir($dirPath), array('..','.'));
    foreach ($files as $file) {
        if (is_dir($dirPath.$file)) {
            deleteDir($dirPath.$file);
        } else {
            unlink($dirPath.$file);
        }
    }
    rmdir($dirPath);
}

function removeEngineJs($str, $find = 'enginejs', $tag = 'script')
{
  while(strpos($str, $find))
  {
    $pos1 = strrpos($str,$tag, strpos($str, $find)-strlen($str))-1;
    $pos2 = strpos($str, $tag, strpos($str, $find))+strlen($tag)+1;

    $str2 = substr($str,$pos1,$pos2-$pos1);

    $str = str_replace($str2,'',$str);
  }
  return $str;
}

function checkName($dir, $name, $ext = 'html')
{
  $name = strtolower(str_replace(' ','-',$name));
	$name = strtolower(str_replace('%','',$name));
	$name = strtolower(str_replace('(','',$name));
	$name = strtolower(str_replace(')','',$name));
	$name = strtolower(str_replace('/','',$name));
	$name = strtolower(str_replace('+','',$name));
	$ret = '';
	for($i=0;$i<strlen($name);$i++)
	{
		if(is_numeric($name[$i]))
		{
			$ret .= $name[$i];
		}
		if($name[$i] == '-')
		{
			$ret .= $name[$i];
		}
		if($name[$i] >= 'A' && $name[$i] <= 'Z')
		{
			$ret .= $name[$i];
		}
		if($name[$i] >= 'a' && $name[$i] <= 'z')
		{
			$ret .= $name[$i];
		}
	}
  $addition = 1;
  while(file_exists($dir.'/'.$ret.'.'.$ext))
  {
    $ret .= $addition;
    $addition++;
  }
  return $ret.'.'.$ext;
}

function fillTemplate($filter, $template, $content)
{
  $result = '';
  $attr = 'class';
  if($filter[0] == '#')
  {
    $filter = str_replace("#","",$filter);
    $attr = 'id';
  }
  else {
    $filter = str_replace(".","",$filter);
  }
  $okay = false;
  $pos = 0;
  $offset = 0;
  $prevoffset = 0;
  $itr = 0;
  while(!$okay && $itr < 100 && $prevoffset <= $offset)
  {
    $pos = strpos($template,$filter, $offset);
    $subs = substr($template,0,$pos);
    $posq = strrpos($subs,'"');
    $poss = strrpos($subs,' ',$posq-strlen($subs))+1;
    if(substr($subs,$poss,$posq-$poss-1) != $attr)
    {
      $prevoffset = $offset;
      $offset = $pos+strlen($filter);
    }
    else {
      $okay = true;
    }
    $itr++;
  }
  if($pos !== false)
  {
    $pos = strpos($template, '>',$pos)+1;
    $result = substr($template,0,$pos).PHP_EOL;
    $result .= $content.PHP_EOL;
    $result .= substr($template,$pos);
  }
  return $result;
}
?>
