<?php
  require_once('functions.php');
  require_once('config.php');

  $list = array();
  if(file_exists('./../enginejs/list.js'))
  {
    $handle = fopen('./../enginejs/list.js', "rb");
    $list_str = '';

    while (!feof($handle)) {
        $list_str .= fread($handle, 8192);
    }
    fclose($handle);
    $list_str = str_replace('var lists = ','',$list_str);
    $list_str = str_replace(';','',$list_str);
    $list_str = str_replace('\n','',$list_str);
    $list_str = str_replace('\t','',$list_str);
    // $list_str = str_replace('\t','',$list_str);
    $list_strs = explode(' ',$list_str);
    $ls = '';
    foreach($list_strs as $s)
    {
      $ls .= trim($s);
    }
    $list = json_decode($ls);
  }

  $templates = array_diff(scandir($config['root']), array_merge(array('..', '.','config.rb','README.md','prepros.cfg','README.html','styleguide.html','.git','.gitignore'),$config['file_excluded']));

  if(is_dir($config['default_export_path']))
  {
    if($config['overwrite'])
    {
      deleteDir($config['default_export_path']);
    }
    else {
      $basic = $config['default_export_path'];
      $itr = 1;
      while(is_dir($config['default_export_path'].'_'.$itr))
      {
        $itr++;
      }
      $config['default_export_path'] = $config['default_export_path'].'_'.$itr;
    }
    mkdir($config['default_export_path'],0755, true);
  }
  else {
    mkdir($config['default_export_path'],0755, true);
  }

  foreach($templates as $t)
  {
    if(is_dir($config['root'].'/'.$t))
    {
      xcopy($config['root'].'/'.$t,$config['default_export_path'].'/'.$t,0755, $config['file_excluded']);
    }
    else if(strrpos($t, '.html') === false){
      copy($config['root'].'/'.$t,$config['default_export_path'].'/'.$t);
    }
    else {
      $handle = fopen($config['root'].'/'.$t, "rb");
      if (FALSE === $handle) {
          exit("Failed to open FILE");
      }
      $template = '';
      while (!feof($handle)) {
          $template .= fread($handle, 8192);
      }
      fclose($handle);

      if(strpos($template,'enginejs') === false)
      {
        copy($config['root'].'/'.$t,$config['default_export_path'].'/'.$t);
      }
      else {

        foreach($list as $l)
        {
          $path = $l;
          $ps = explode('?',$l->path);
          $l->template = $ps[0];
          $l->view = $config['view'].'.'.$config['extension'];
          if(count($ps) > 1)
          {
            $l->view = str_replace('view=','',$ps[1]).'.'.$config['extension'];
          }
        }
        $template = removeEngineJs($template);
        // $template_s = substr($template,0, strpos($template,''));
        if($config['usingtemplate'])
        {
          $handle = fopen($config['default_export_path'].'/list.txt', "a");
          foreach($list as $l)
          {
            if(strpos($l->path, $t) !== false)
              fwrite($handle,$l->name.PHP_EOL.'TEMPLATE : '.$l->template.PHP_EOL.'VIEW : '.$config['viewsfolder'].'/'.$l->view.PHP_EOL.PHP_EOL);
          }
          fclose($handle);

          $handle = fopen($config['default_export_path'].'/'.$t, "a");
          fwrite($handle,$template);
          fclose($handle);
        }
        else {
          foreach($list as $l)
          {
            if(strpos($l->path, $t) !== false)
            {
              if(file_exists($config['root'].'/'.$config['viewsfolder'].'/'.$l->view))
              {
                $content = '';
                $handle = fopen($config['root'].'/'.$config['viewsfolder'].'/'.$l->view, "rb");

                while (!feof($handle)) {
                    $content .= fread($handle, 8192);
                }
                fclose($handle);
                $name = checkName($config['default_export_path'], $l->name, 'html');
                $l->filename = $name;
                $result = fillTemplate($config['container'],$template, $content);
                $handle = fopen($config['default_export_path'].'/'.$name, "a");
                fwrite($handle,$result);
                fclose($handle);
              }
            }
          }
        }
      }
    }
  }


  if(!$config['usingtemplate'])
  {
    if(is_dir($config['default_export_path'].'/'.$config['viewsfolder']))
      deleteDir($config['default_export_path'].'/'.$config['viewsfolder']);
    if(!file_exists($config['default_export_path'].'/'.'index.html') && count($list) > 1)
    {
      $l = $list[0];
      rename($config['default_export_path'].'/'.$l->filename,$config['default_export_path'].'/'.'index.html');
    }
  }
?>
