<?php
  $dir = dirname(__FILE__);
	$path = substr($dir,0,strrpos($dir, '/'));
	$path = $path ? $path: substr($dir,0,strrpos($dir, '\\'));
  $config = array(
    'viewsfolder' => 'views',
    'container' => '.content',
    'view'=>'home',
    'extension'=>'html',
    'root' => $path,
    'file_excluded' => array(
      'css/src',
      'enginejs',
      'exporter',
      'styleguide',
      'config.rb',
      'crossdomain.xml',
      'README.md',
      'README.html',
      'styleguide.html',
      '.git',
      '.gitignore',
      '.htaccess',
			'prepros.cfg'
    ),
    'default_export_path' => $path.'export',
    'usingtemplate' => true,
    'overwrite' => true
  );
?>
